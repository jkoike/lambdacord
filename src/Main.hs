{-# LANGUAGE OverloadedStrings, RecordWildCards, MultiWayIf #-}
import System.Environment
import Data.Text as T hiding (intercalate)
import GHC.Stats
import Control.Applicative ((<|>))
import Data.List (intercalate)
import System.Process
import System.Timeout
import System.Exit (ExitCode(..))
import Control.Concurrent (threadDelay)

import Network.Socket (Family(AF_INET))
import Pointfree
import Pipes
import Data.Attoparsec.Text
import Data.Maybe
import Data.Either
import Data.Version (showVersion)
import Language.Haskell.Interpreter
import System.Log.Logger
import System.Log.Handler.Syslog

import Network.Discord
import Paths_lambdacord (version)

reply :: Message -> Text -> Effect DiscordM ()
reply Message{messageChannel=chan} cont = fetch' $ CreateMessage chan cont Nothing

codeblock :: Parser (Either Text (Text, Text))
codeblock = (do
      skipWhile (/= '`') 
      string "```"
      lang <- takeTill ( == '\n')
      char '\n'
      block <- takeTill ( == '`')
      string "```"
      return $ Right (lang, block))
  <|> (do
      skipWhile (/= '`') 
      char '`'
      block <- takeTill ( == '`')
      char '`'
      return $ Left block)

getBlock :: Monad m => Either Text (Text, Text) -> m Text
getBlock (Right (_, block)) = return block
getBlock (Left block)       = return block

main :: IO ()
main = do
  token <- getEnv "DISCORD_TOKEN"
  host  <- getEnv "LOGSTASH_HOST"
  port  <- getEnv "LOGSTASH_PORT"
  log <- openlog_remote AF_INET host (read port) "Lambdacord" [] USER INFO
  updateGlobalLogger rootLoggerName (addHandler log . setLevel INFO)
  runBot (Bot token) $ do
    with ReadyEvent $ \_ -> liftIO $ putStrLn "Connected"
    with MessageCreateEvent $ \msg@Message{..} ->
      when (not . userIsBot $ messageAuthor) $
        if | ">pf" `isPrefixOf` messageContent -> do
              case pointfree' . unpack =<< getBlock =<< (maybeResult $ parse codeblock messageContent) of
                Nothing -> reply msg "Couldn't pointfree your expression"
                Just pf -> reply msg . pack $ "```haskell\n" ++ pf ++ "\n```"
           | ">eval" `isPrefixOf` messageContent -> do
              block <- case (fromMaybe (Left "\"Couldn\'t parse your codeblock\"") . maybeResult $ parse codeblock messageContent) of
                Right ("haskell", block) -> return block
                Right ("hs", block) -> return block
                Left block -> return block
                otherwise -> return "\"Please use `haskell` or `hs`\""
              res <- liftIO . mueval $ unpack block
              reply msg $ pack $ "```haskell\n" ++ res ++ "\n```"
           | ">t" `isPrefixOf` messageContent -> do
              case maybeResult $ parse codeblock messageContent of
                Nothing -> reply msg "usage: >t `codeblock`"
                Just block -> do
                  code <- case block of
                    Right ("haskell", block) -> return block
                    Right ("hs", block) -> return block
                    Left block -> return block
                  res <- liftIO . runInterpreter $ setImports ["Prelude"] 
                    >> (typeOf $ unpack code)
                  reply msg . pack $ either showError id res
           | ">info" `isPrefixOf` messageContent -> do
              GCStats{..} <- liftIO $ getGCStats
              reply msg . pack $ intercalate "\n" [
                  "```yaml"
                , "Lambdacord: https://gitlab.com/jkoike/lambdacord"
                , "Version: " ++ showVersion version
                , "Peak memory: " ++ show maxBytesUsed
                , "Current memory: " ++ show currentBytesUsed
                , "Uptime: " ++ show wallSeconds
                , "CPU time: " ++ show cpuSeconds
                , "GC CPU time: " ++ show gcCpuSeconds
                , "```"
                ]
           | ">hoogle" `isPrefixOf` messageContent -> do
              block <- case (fromMaybe (Left "\"Couldn\'t parse your codeblock\"") . maybeResult $ parse codeblock messageContent) of
                Right ("haskell", block) -> return block
                Right ("hs", block) -> return block
                Left block -> return block
                otherwise -> return "\"Please use `haskell` or `hs`\""
              res <- liftIO . hoogle $ unpack block
              reply msg $ pack $ "```haskell\n" ++ res ++ "\n```"
           | otherwise -> return ()
      where
        showError (WontCompile [GhcError {errMsg=err}]) = err
        showError _ = "No."
        hoogle :: String -> IO String
        hoogle query = do
          (code, stdout, stderr) <- readProcessWithExitCode "hoogle" [query] ""
          if code == ExitSuccess
            then return stdout
            else return stderr
        mueval query = do
          (code, stdout, stderr) <- readProcessWithExitCode "eval" [query] ""
          if code == ExitSuccess
            then return stdout
            else return ("Runtime Error: " ++ stderr)
